import os
import random
from time import sleep
import higher_lower_dependencies

SCORE = 0

# Function to clear the console screen
def clear_screen():
    if os.name == 'posix':  # for Mac and Linux
        _ = os.system('clear')
    else:  # for Windows
        _ = os.system('cls')

def random_data():
    random.shuffle(data)
    datas = random.choice(data)
    return datas

def format(account):
    name = account['name']
    description = account['description']
    country = account['country']
    return f"{name}: a {description}, from {country}."

def compare(count1, count2, guess):
    if count1 > count2:
        return guess == 'a'
    else:
        return guess == 'b'
    
print("""
Hola! This is a simplified version of the Higher or Lower game, but with a twist: you'll be guessing whose Instagram account has more followers than the others.\n
Before starting the game, let's go over the scoring rules:
\tFor every correct guess, you'll be awarded +5 points!
\tFor every incorrect guess, you'll lose -2 points, but don't worry - your score will never go below 0 points!
\tThe follower's data is upto date till 7th April, 2024.
Good luck and have fun!
""")

replay = True
ran_2 = random_data()

while replay:
    ran_1 = ran_2
    ran_2 = random_data()
    while ran_1 == ran_2:
        ran_2 = random_data()

    follow_a = ran_1['follower_count']
    follow_b = ran_2['follower_count']

    print(f'Total score: {SCORE}\n')

    print(f"Compare A: {format(ran_1)}")
    print(vs)
    print(f"Against B: {format(ran_2)}")

    guess = input(f"""\nWho has more followers? Type,
        1. 'A' for {ran_1['name']}
        2. 'B' for {ran_2['name']}\nYour pick: """).lower()

    correct = compare(follow_a, follow_b, guess)

    if correct:
        SCORE += 5
        print(f"\nYou're right!\nCurrent score: {SCORE}")
        sleep(2)
        clear_screen()
    else:
        SCORE -= 2
        if SCORE < 0:
            SCORE = 0
        print(f"\nOh no! That's not correct.")
        sleep(3)
        clear_screen()
        print(f"That was a great game! After that amazing game, your total score is {SCORE}!")
        replay = False