# Higher or Lower Game

Welcome to the Higher or Lower Game, a fun and interactive console game where you guess which Instagram account has more followers!

## How to Play

- You will be presented with two Instagram accounts.
- Guess which account has more followers by typing 'A' or 'B'.
- For every correct guess, you earn +5 points.
- For every incorrect guess, you lose -2 points (but you can't go below 0).

## Scoring Rules

- **Correct Guess:** +5 points
- **Incorrect Guess:** -2 points (score won't go below 0)
- The follower data is up-to-date as of April 7, 2024.